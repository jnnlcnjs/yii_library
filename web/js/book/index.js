$(function(){
	var bookIndex = new bookIndex();

	bookIndex._init();
	function bookIndex(){
		var self = this;

		self.bindBookIndexData = function(){
			$('#book_list_table').DataTable( {
		        ajax: "get-book-list",
		        responsive: true,
		        // dom: 'Bfrtip',
		        pageLength : 5,
		    	lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']],
		        'deferRender': true,
		        "columns": [
		            { "data": "id" },
		            { "data": "name" },
		            { "data": "description" },
		            { "data": "author_name" },
		            { "data": "genre.name" },
		            { "data": "section.name" },
		            {
		            	"data": null,
		            	'orderable': false, 
		            	'searchable': false,
		            	"render": function(data,type,full,meta)
						{
							return '<a href="view?id='+data.id+'"><span class="glyphicon glyphicon-eye-open"></span></a>'+
								   '<a href="update?id='+data.id+'"><span class="glyphicon glyphicon-pencil"></span></a>'+
								   '<a href="borrow-book?id='+data.id+'"><span class="glyphicon glyphicon-book"></span></a>';
						}
		            }
		        ],
		    } );
		}

		self._init = function(){
			self.bindBookIndexData();
		}
	}
});