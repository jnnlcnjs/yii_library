$(function(){
	var sectionIndex = new sectionIndex();

	sectionIndex._init();
	function sectionIndex(){
		var self = this;

		self.bindSectionIndexData = function(){
			$('#section_list_table').DataTable( {
		        ajax: "get-section-list",
		        responsive: true,
		        // dom: 'Bfrtip',
		        pageLength : 5,
		    	lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']],
		        'deferRender': true,
		        "columns": [
		            { "data": "id" },
		            { "data": "name" },
		            { "data": "description" },
		            {
		            	"data": null,
		            	'orderable': false, 
		            	'searchable': false,
		            	"render": function(data,type,full,meta)
						{
							return '<a href="view?id='+data.id+'"><span class="glyphicon glyphicon-eye-open"></span></a>'+'<a href="update?id='+data.id+'"><span class="glyphicon glyphicon-pencil"></span></a>';
						}
		            }
		        ],
		    } );
		}

		self._init = function(){
			self.bindSectionIndexData();
		}
	}
});