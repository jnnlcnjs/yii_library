$(function(){
	var clientIndex = new clientIndex();

	clientIndex._init();
	function clientIndex(){
		var self = this;

		self.bindClientIndexData = function(){
			$('#client_list_table').DataTable( {
		        ajax: "get-client-list",
		        responsive: true,
		        // dom: 'Bfrtip',
		        pageLength : 5,
		    	lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']],
		        'deferRender': true,
		        "columns": [
		            { "data": "id" },
		            { "data": "firstname" },
		            { "data": "lastname" },
		            { "data": "email" },
		            {
		            	"data": null,
		            	'orderable': false, 
		            	'searchable': false,
		            	"render": function(data,type,full,meta)
						{
							return '<a href="view?id='+data.id+'"><span class="glyphicon glyphicon-eye-open"></span></a>'+
							'<a href="update?id='+data.id+'"><span class="glyphicon glyphicon-pencil"></span></a>'
						}
		            }
		        ],
		    } );
		}

		self._init = function(){
			self.bindClientIndexData();
		}
	}
});