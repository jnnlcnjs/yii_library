$(function(){
	var borrowIndex = new borrowIndex();

	borrowIndex._init();
	function borrowIndex(){
		var self = this;

		self.bindBorrowIndexData = function(){
			$('#borrow_list_table').DataTable( {
		        ajax: "get-borrow-list",
		        responsive: true,
		        // dom: 'Bfrtip',
		        pageLength : 5,
		    	lengthMenu: [[5, 10, 20, -1], [5, 10, 20, 'All']],
		        'deferRender': true,
		        "columns": [
		            { "data": "id" },
		            { 
		            	"data": null,
		            	"render": function(data,type,full,meta)
		            	{
							return data.client.firstname + " " +data.client.lastname;
						}
		            },
		            { "data": "book.name" },
		            { "data": "book.description" },
		            { "data": "book.author_name" },
		            { "data": "book.genre.name" },
		            { "data": "book.section.name" },
		            { 
		            	"data": null,
		            	"render": function(data,type,full,meta)
		            	{
							return data.status == 1?"Borrowed":"Returned";
						}
		            },
		            {
		            	"data": null,
		            	'orderable': false, 
		            	'searchable': false,
		            	"render": function(data,type,full,meta)
						{
							return data.status == 1?'<a href="javascript:void(0)" data-id="'+data.id+'" class="return_book_btn" title="return book"><span class="glyphicon glyphicon-hand-left"></span></a>':"";
						}
		            }
		        ],
		    } );

		    $(document).on('click','.return_book_btn',function(){
		    	var id = $(this).attr("data-id");
				var csrfToken = $('meta[name="csrf-token"]').attr("content");

		    	swal({
				  title: "Warning",
				  text: "Are you sure you want to return the book?",
				  type: "info",
				  showCancelButton: true,
				  closeOnConfirm: false,
				  showLoaderOnConfirm: true
				}, function () {
					$.ajax({
						url: "return-book?id="+id,
						method: "POST",
						data: {_csrf:csrfToken},
						dataType: "json",
			           	success: function(data) {
			           		if(data.success == 1){
			           			$('#borrow_list_table').DataTable().ajax.reload(null,false);
			           			swal("Book successfully returned");
			           		}
			           	}
					})
				});
		    });
		}

		self._init = function(){
			self.bindBorrowIndexData();
		}
	}
});