<?php

namespace app\models;

use Yii;
use app\models\Client;
use app\models\Book;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $book_id
 * @property int $client_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['book_id', 'client_id'], 'integer'],
            [['book_id', 'client_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'client_id' => 'Client ID',
        ];
    }

    public function getClient()
    {
        return $this->hasOne(Client::className(),['id' => 'client_id']);
    }

    public function getBook()
    {
        return $this->hasOne(Book::className(),['id' => 'book_id']);
    }
}
