<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $auth_key
 */

class User extends ActiveRecord implements IdentityInterface{
    const SCENARIO_UPDATE = "update_user";

    public static function tableName(){
        return 'user';
    }

    public function rules(){
        return [
            [['username','password','email'],'required','on' => self::SCENARIO_DEFAULT],
            [['username','email'],'required','on' => self::SCENARIO_UPDATE],
            ['password','safe','on' => self::SCENARIO_UPDATE],
            ['email','email'],
            [['username'], 'checkUniqueUsername','on' => self::SCENARIO_DEFAULT],
            [['username'], 'safe','on' => self::SCENARIO_UPDATE],
        ];
    }

    public function checkUniqueUsername($attribute,$params){ 
        $user =  self::find()->where(['username'=>$this->username])->one();
        if(isset($user) && $user != null ){
            $this->addError($attribute,'Username already exist.');
        }
    }

    public function findByUsername($username){
        return static::findOne(['username' => $username]);
    }

    public static function findIdentity($id){
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        return $this->auth_key = \Yii::$app->security->generateRandomString();
    }
}

?>