<?php

namespace app\models;

use Yii;
use yii\db\Query;


/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 */
class Client extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'email'], 'string', 'max' => 255],
            [['firstname', 'lastname', 'email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
        ];
    }

    public function suggestClient($q = null){
        $query = new Query;
        $query->select('id, concat_ws(" ",`firstname`,`lastname`) as text')
              ->from('client')
              ->where(['is_deleted' => 0])
              ->andWhere(['or',['like','firstname',$q],['like','lastname',$q]])
              // ->orFilterWhere(['like','lastname',$q])
              ->limit(20);

        $command = $query->createCommand();
        return $command->queryAll();
    }
}
