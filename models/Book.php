<?php

namespace app\models;

use Yii;
use app\models\Genre;
use app\models\Section;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $author_name
 * @property int $genre_id
 * @property int $section_id
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['genre_id', 'section_id'], 'integer'],
            [['genre_id', 'section_id'], 'required'],
            [['client_id'], 'required', 'on' => 'get_book'],
            [['name', 'description', 'author_name'], 'string', 'max' => 255],
            [['name', 'description', 'author_name'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'author_name' => 'Author Name',
            'genre_id' => 'Genre ID',
            'section_id' => 'Section ID',
            'client_id' => 'Client ID',
        ];
    }

    public function getGenre()
    {
        return $this->hasOne(Genre::className(),['id' => 'genre_id']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(),['id' => 'section_id']);
    }
}
