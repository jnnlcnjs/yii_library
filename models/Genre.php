<?php

namespace app\models;

use Yii;
use yii\db\Query;


/**
 * This is the model class for table "genre".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 */
class Genre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }

    public function suggestGenre($q = null){
        $query = new Query;
        $query->select('id, name AS text')
              ->from('genre')
              ->where(['like','name',$q])
              ->andWhere(['is_deleted' => 0])
              ->limit(20);

        $command = $query->createCommand();
        return $command->queryAll();
    }
}
