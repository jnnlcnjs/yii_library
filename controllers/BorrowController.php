<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\Genre;
use app\models\Transaction;
use app\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\db\Query;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BorrowController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {

        return $this->render('index', [
        ]);
    }

    //fetch data to populate list
    public function actionGetBorrowList(){
      $borrow['data'] = Transaction::find()->orderBy(['transaction.id'=>SORT_ASC])
                                           ->joinWith('client')
                                           ->joinWith('book')
                                           ->joinWith('book.genre')
                                           ->joinWith('book.section')
                                           ->asArray()
                                           ->all();


      Yii::$app->response->format = Response::FORMAT_JSON;
      return $borrow;
    }

    //return the book
    public function actionReturnBook($id){
      $model = Transaction::findOne($id);
      $model->status = 0;
      $model->save();

      $book = Book::findOne($model->book_id);
      $book->is_borrowed = 0;
      $book->save();
      Yii::$app->response->format = Response::FORMAT_JSON;
      $message = array("success"=>1);
      return $message;
    }

}
