<?php

namespace app\controllers;

use Yii;
use app\models\Book;
use app\models\Genre;
use app\models\Client;
use app\models\Section;
use app\models\Transaction;
use app\models\BookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\db\Query;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Book models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Book model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $data = Book::find()->joinWith('genre')
                            ->joinWith('section')
                            ->where(['book.id' => $id])
                            ->one();

        return $this->render('view', [
            'model' => $data,
        ]);
    }

    /**
     * Creates a new Book model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        $model = $this->findModel($id);
        $data = Book::find($id)->joinWith('genre')
                                ->joinWith('section')
                                ->asArray()
                                ->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
        ]);
    }

    /**
     * Deletes an existing Book model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    //fetch all data of genres
    public function actionGetBookList(){
        $book["data"] = Book::find()->joinWith('genre')
                                    ->joinWith('section')
                                    ->where(['is_borrowed' => 0])
                                    ->andWhere(['book.is_deleted' => 0])
                                    ->asArray()
                                    ->all();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $book;
    }

    //suggest genre
    public function actionSuggestGenre($q = null, $id = null){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if(!is_null($q)){
            $out['results'] = Genre::suggestGenre($q);
        }elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Genre::find($id)->name];
        }

        return $out;
    }

    //suggest section
    public function actionSuggestSection($q = null, $id = null){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if(!is_null($q)){
            $out['results'] = Section::suggestSection($q);
        }elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Section::find($id)->name];
        }

        return $out;
    }

    //get the book
    public function actionBorrowBook($id){
        $data = Book::find()->joinWith('genre')
                            ->joinWith('section')
                            ->where(['book.id'=>$id])
                            ->one();

        $model = new Transaction();
        $model->date_created = date("Y-m-d");
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $data->is_borrowed = 1;//update book status to borrowed
            $data->save();
            return $this->redirect('/borrow/index');
        }

        return $this->render('borrow', [
            'data' => $data,
            'model' => $model,
        ]);
    }

    //suggest client
    public function actionSuggestClient($q = null, $id = null){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];

        if(!is_null($q)){
            $out['results'] = Client::suggestClient($q);
        }elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Client::find($id)->name];
        }

        return $out;
    }
}
