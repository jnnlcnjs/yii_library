<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;

	$this->title = 'Create Account';
	$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
	<div class="col-lg-5">
		<h1><?= Html::encode($this->title) ?></h1>
		<p>Please fill out the following fields to register:</p>
		
		<?php
			$form = ActiveForm::begin(['id' => 'registration-form', 'enableAjaxValidation' => true]);
		?>
			<?=$form->field($model,'username')?>
			<?=$form->field($model,'email')->input('email')?>
			<?=$form->field($model,'password')->passwordInput()?>

		<div class="form-group">
			<?=Html::submitButton("Register",["class" => "btn btn-primary","name" => "register-btn"])?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>	
</div>
