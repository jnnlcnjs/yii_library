<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
\app\assets\BookAsset::register($this);
use kartik\select2\Select2;

$this->title = 'Book List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container-fluid">
    <?=Html::a("Create Book","create",["class" => "btn btn-primary pull-right"])?><br><br>
</div>
<div id="book_list_container">
    <?=$this->render('book_list');?>
</div>