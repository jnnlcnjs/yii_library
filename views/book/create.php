<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = 'Create Book';
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$url_genre = \yii\helpers\Url::to(['suggest-genre']);
$url_section = \yii\helpers\Url::to(['suggest-section']);

?>
<div class="book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(['id' => 'create_book_form']) ?>
    	<?=$form->field($model,'name')?>
    	<?=$form->field($model,'description')?>
    	<?=$form->field($model,'author_name')?>

    	<?= $form->field($model, 'genre_id')->widget(Select2::classname(), [
		    'options' => ['placeholder' => 'Select a genre ...'],
		    'pluginOptions' => [
		        'allowClear' => true,
			    'minimumInputLength' => 1,
			    'language' => [
			        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
			    ],
			    'ajax' => [
			        'url' => $url_genre,
			        'dataType' => 'json',
			        'data' => new JsExpression('function(params) { return {q:params.term}; }')
			    ],
			    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			    'templateResult' => new JsExpression('function(book) { return book.text; }'),
			    'templateSelection' => new JsExpression('function (book) { return book.text; }'),
		    ],
		]);
		?>

		<?= $form->field($model, 'section_id')->widget(Select2::classname(), [
		    'options' => ['placeholder' => 'Select a section ...'],
		    'pluginOptions' => [
		        'allowClear' => true,
			    'minimumInputLength' => 1,
			    'language' => [
			        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
			    ],
			    'ajax' => [
			        'url' => $url_section,
			        'dataType' => 'json',
			        'data' => new JsExpression('function(params) { return {q:params.term}; }')
			    ],
			    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			    'templateResult' => new JsExpression('function(book) { return book.text; }'),
			    'templateSelection' => new JsExpression('function (book) { return book.text; }'),
		    ],
		]);
		?>

		<div class="form-group">
			<?=Html::submitButton("Submit",["class" =>"btn btn-primary"])?>
		</div>
    <?php ActiveForm::end()?>

</div>
