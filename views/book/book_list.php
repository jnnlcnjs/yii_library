<div class="table-responsive">
	<table id="book_list_table" class="table table-bordered table-hover display responsive no-wrap" style="width:100%">
		<thead>
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>DESCRIPTION</th>
				<th>AUTHOR</th>
				<th>GENRE</th>
				<th>SECTION</th>
				<th>ACTION</th>
			</tr>
		</thead>
	</table>	
</div>