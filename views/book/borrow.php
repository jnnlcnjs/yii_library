<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = "Borrow Book";
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$url_client = \yii\helpers\Url::to(['suggest-client']);
?>
<div class="book-create">

    <h1><?= Html::encode("Book Details") ?></h1>

    <?= DetailView::widget([
        'model' => $data,
        'attributes' => [
            'id',
            'name',
            'description',
            'author_name',
            [
                'label' => 'Genre Name',
                'value' => $data->genre->name
            ],
            [
                'label' => 'Section Name',
                'value' => $data->section->name
            ],
        ],
    ]) ?>

    <?php $form = ActiveForm::begin(['id' => 'borrow_book_form']) ?>
    	<?= $form->field($model,'book_id')->hiddenInput(['value' => $data->id])->label(false);?>

		<?= $form->field($model, 'client_id')->widget(Select2::classname(), [
		    'options' => ['placeholder' => 'Select a section ...'],
			// 'initValueText' => $data['section']['name'],
		    'pluginOptions' => [
		        'allowClear' => true,
			    'minimumInputLength' => 1,
			    'language' => [
			        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
			    ],
			    'ajax' => [
			        'url' => $url_client,
			        'dataType' => 'json',
			        'data' => new JsExpression('function(params) { return {q:params.term}; }')
			    ],
			    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
			    'templateResult' => new JsExpression('function(book) { return book.text; }'),
			    'templateSelection' => new JsExpression('function (book) { return book.text; }'),
		    ],
		]);
		?>

		<div class="form-group">
			<?=Html::submitButton("Submit",["class" =>"btn btn-primary"])?>
		</div>
    <?php ActiveForm::end()?>

</div>
