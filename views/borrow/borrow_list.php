<div class="table-responsive">
	<table id="borrow_list_table" class="table table-bordered table-hover display responsive no-wrap" style="width:100%">
		<thead>
			<tr>
				<th>ID</th>
				<th>CLIENT NAME</th>
				<th>BOOK NAME</th>
				<th>BOOK DESCRIPTION</th>
				<th>BOOK AUTHOR</th>
				<th>BOOK GENRE</th>
				<th>BOOK SECTION</th>
				<th>STATUS</th>
				<th>ACTION</th>
			</tr>
		</thead>
	</table>	
</div>