<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
\app\assets\BorrowAsset::register($this);
use kartik\select2\Select2;

$this->title = 'Borrowed Book List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="borrow_list_container">
    <?=$this->render('borrow_list');?>
</div>