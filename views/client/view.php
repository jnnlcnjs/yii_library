<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->firstname." ".$model->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstname',
            'lastname',
            'email:email',
        ],
    ]) ?>

    <div>
        <h3>Library History</h3>
        <?php
            \yii\widgets\Pjax::begin();
               echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'label'=>'Book',
                            'value'=>'book.name'
                        ],
                        [
                            'label'=>'Status',
                            'value'=> function($model,$key,$index)
                            {
                                return $model->status == 1?"Borrowed":"Returned";
                            }
                        ],
                    ]
               ]);
           \yii\widgets\Pjax::end();
        ?>
    </div>
</div>
