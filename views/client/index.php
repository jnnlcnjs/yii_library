<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
\app\assets\ClientAsset::register($this);

$this->title = 'Client List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container-fluid">
    <?=Html::a("Create Client","create",["class" => "btn btn-primary pull-right"])?><br><br>
</div>
<div id="client_list_container">
    <?=$this->render('client_list');?>
</div>