<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
\app\assets\SectionAsset::register($this);

$this->title = 'Section List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container-fluid">
	<?=Html::a("Create Section","create",["class" => "btn btn-primary pull-right"])?><br><br>
</div>
<div id="section_list_container">
	<?=$this->render('section_list');?>
</div>