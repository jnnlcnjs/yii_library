<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
\app\assets\GenreAsset::register($this);

$this->title = 'Genre List';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row container-fluid">
    <?=Html::a("Create Genre","create",["class" => "btn btn-primary pull-right"])?><br><br>
</div>
<div id="genre_list_container">
    <?=$this->render('genre_list');?>
</div>